import mongoose from "mongoose";
import { JobCategoryEnum } from "./JobOffer.enum";

/**
 * Job offer interface
 */
export interface IJobOffer extends mongoose.Document {
  /**
   * Category
   */
  category: JobCategoryEnum;
  /**
   * Expire date
   */
  endDate: Date | string;
  /**
   * Offering company name
   */
  offeringCompanyName: string;
  /**
   * Start date
   */
  startingDate: Date | string;
  /**
   * Title
   */
  title: string;
  /**
   * User/Owner id
   */
  ownerId: string;
}

/**
 * Job offer schema
 */
export const JOB_OFFER_SCHEMA = new mongoose.Schema({
  category: { type: JobCategoryEnum, required: true },
  endDate: { type: Date, required: true },
  offeringCompanyName: { type: String, required: true },
  startingDate: { type: Date, required: true },
  title: { type: String, required: true },
  ownerId: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
});

/**
 * Job offer model
 */
const JOB_OFFER = mongoose.model<IJobOffer>("JobOffer", JOB_OFFER_SCHEMA);
export default JOB_OFFER;
