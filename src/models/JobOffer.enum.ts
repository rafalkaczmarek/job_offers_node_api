/**
 * ENUM that contains available job categories.
 */
export enum JobCategoryEnum {
  /**
   * IT
   */
  it = "it",
  /**
   * Food & Drinks
   */
  food_and_drinks = "food_and_drinks",
  /**
   * Office
   */
  office = "office",
  /**
   * Courier
   */
  courier = "courier",
  /**
   * Shop assistant
   */
  shop_assistant = "shop_assistant"
}
