import { Request, Response } from "express";
import { check, validationResult } from "express-validator/check";
import moment from "moment";
import mongoose from "mongoose";
import { AppSettings } from "../AppSettings";
import { IJobOffer } from "../models/JobOffer";
import { JobCategoryEnum } from "../models/JobOffer.enum";
import { IUser } from "../models/User";
import logger from "../utilities/logger";
import customValidators from "./customValidators/customValidators";

const USER = mongoose.model("User");
const JOB_OFFER = mongoose.model("JobOffer");

/**
 * List all users
 * @param req
 * @param res
 */
const listAllUsers = (req: Request, res: Response) => {
  USER.find({}, (err, users: IUser[]) => {
    if (err) {
      return res.status(400).send(err);
    }

    users.forEach((user: IUser) => {
      user.creationDate = moment
        .utc(user.creationDate)
        .format(AppSettings.DATE_TIME_SERVER_FORMAT);
    });
    return res.json(users);
  });
};

/**
 * Create user validation
 */
const createUserValidation = [
  check("login").isEmail(),
  check("password")
    .not()
    .isEmpty(),
  check("creationDate")
    .custom(customValidators.isValidDate)
    .withMessage(AppSettings.DATE_IS_NOT_VALID_MESSAGE)
];

/**
 * Create user
 * @param req
 * @param res
 */
const createUser = (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const user: IUser = req.body;
  user.creationDate = moment.utc(user.creationDate).toDate();

  USER.findOne(
    { login: user.login },
    (findExistingError, existingUser: IUser) => {
      if (findExistingError) {
        return res.status(400).send(findExistingError);
      }

      if (existingUser) {
        return res
          .status(409)
          .json({ message: "User with that login already exists." });
      }

      const newUser = new USER(user);
      newUser.save((err: any, newUserSaved: IUser) => {
        if (err) {
          return res.status(400).send(err);
        }
        return res.status(201).json(newUserSaved);
      });
    }
  );
};

/**
 * Read user
 * @param req
 * @param res
 */
const readUser = (req: Request, res: Response) => {
  USER.findById(req.params.userId, (err: any, user: IUser) => {
    if (err) {
      return res.status(400).send(err);
    }

    logger.info(user);
    if (!user) {
      return res.status(404).json({ message: "User with that id not exists." });
    }

    return res.json(user);
  });
};

/**
 * Update user
 * @param req
 * @param res
 */
const updateUser = (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const user: IUser = req.body;
  user.creationDate = moment.utc(user.creationDate).toDate();

  USER.findOne(
    { login: user.login },
    (findExistingError, existingUser: IUser) => {
      if (findExistingError) {
        return res.status(400).send(findExistingError);
      }

      if (existingUser) {
        return res
          .status(409)
          .json({ message: "User with that login already exists." });
      }

      USER.findOneAndUpdate(
        { _id: req.params.userId },
        user,
        { new: true },
        (err, newUserSaved: IUser) => {
          if (err) {
            return res.status(400).send(err);
          }
          return res.json(newUserSaved);
        }
      );
    }
  );
};

/**
 * Delete user
 * @param req
 * @param res
 */
const deleteUser = (req: Request, res: Response) => {
  USER.remove(
    {
      _id: req.params.userId
    },
    (err: any) => {
      if (err) {
        return res.status(400).send(err);
      }
      return res.json({ message: "User successfully deleted" });
    }
  );
};

/**
 * Read user offers
 * @param req
 * @param res
 */
const readUserOffers = (req: Request, res: Response) => {
  const requiredCategoriesQuery = req.query.category;

  // TODO: This still can be done nicer :).
  let requiredCategoriesList: string[] = Object.keys(JobCategoryEnum);
  if (requiredCategoriesQuery) {
    requiredCategoriesList = requiredCategoriesQuery
      .split(" ")
      .filter((queryCategory: string) =>
        requiredCategoriesList.some(
          (existingCategory: string) => existingCategory === queryCategory
        )
      );
  }
  logger.info(requiredCategoriesList);

  JOB_OFFER.find(
    {
      ownerId: req.params.userId,
      category: {
        $in: requiredCategoriesList
      }
    },
    (findUserOffersError, userOffers: IJobOffer) => {
      if (findUserOffersError) {
        return res.status(400).send(findUserOffersError);
      }

      return res.json(userOffers);
    }
  );
};

/**
 * Create job offer validation
 */
const createJobOfferValidation = [
  check("title")
    .not()
    .isEmpty(),
  check("offeringCompanyName")
    .not()
    .isEmpty(),
  check("startingDate")
    .custom(customValidators.isValidDate)
    .withMessage(AppSettings.DATE_IS_NOT_VALID_MESSAGE),
  check("endDate")
    .custom(customValidators.isValidDate)
    .withMessage(AppSettings.DATE_IS_NOT_VALID_MESSAGE),
  check("category")
    .custom(customValidators.isValidJobOfferCategory)
    .withMessage("Unrecognized category.")
];

/**
 * Create job offer
 * @param req
 * @param res
 */
const createJobOffer = (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const jobOffer: IJobOffer = req.body;
  const jobStartingDate = moment.utc(jobOffer.startingDate);
  const jobEndDate = moment.utc(jobOffer.endDate);

  if (jobEndDate.isBefore(jobStartingDate)) {
    return res
      .status(422)
      .json({ message: "Job offer cannot expire before it even started." });
  }
  if (jobEndDate.isBefore(moment())) {
    return res.status(422).json({ message: "Cannot add expired job offer." });
  }

  jobOffer.startingDate = jobStartingDate.toDate();
  jobOffer.endDate = jobEndDate.toDate();
  jobOffer.ownerId = req.params.userId;

  // TODO: Before saving job offer check if user exists.

  const newJobOffer = new JOB_OFFER(jobOffer);
  newJobOffer.save((errSave: any, newJobOfferSaved: IJobOffer) => {
    if (errSave) {
      return res.status(400).send(errSave);
    }

    USER.updateOne(
      { _id: req.params.userId },
      { $push: { offers: newJobOffer } },
      (err: any, userUpdated: IUser) => {
        if (err) {
          // TODO: Should job offer be removed then? What if this also fail?
          return res.status(400).send(err);
        }
        return res.status(201).json(newJobOfferSaved);
      }
    );
  });
};

export default {
  createUser,
  createUserValidation,
  deleteUser,
  listAllUsers,
  readUser,
  updateUser,
  createJobOffer,
  createJobOfferValidation,
  readUserOffers
};
