/**
 * Provides settings for the application
 */
export class AppSettings {
  /**
   * Has pattern for date and time format received from server.
   * @type {string}
   */
  public static DATE_TIME_SERVER_FORMAT = "YYYY-MM-DDTHH:mm:ss.SSS";
  /**
   * Has pattern for date from server.
   * @type {string}
   */
  public static DATE_SERVER_FORMAT = "YYYY-MM-DD";
  /**
   * Has pattern for server time format.
   * @type {string}
   */
  public static TIME_SERVER_FORMAT = "HH:mm:ss.SSSSSSS";
  /**
   * Invalid date validation message.
   */
  public static DATE_IS_NOT_VALID_MESSAGE = `The date must be valid and in correct format '${
    AppSettings.DATE_TIME_SERVER_FORMAT
  }'`;
}
