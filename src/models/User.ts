import mongoose from "mongoose";
import { IJobOffer } from "./JobOffer";

/**
 * User interface
 */
export interface IUser extends mongoose.Document {
  /**
   * Login
   */
  login: string;
  /**
   * Password
   */
  password: string;
  /**
   * Job offers
   */
  offers: IJobOffer[];
  /**
   * Creation date
   */
  creationDate: Date | string;
}

/**
 * User schema
 */
export const USER_SCHEMA = new mongoose.Schema({
  creationDate: { type: Date, required: true },
  login: { type: String, required: true },
  offers: [{ type: mongoose.Schema.Types.ObjectId, ref: "JobOffer" }],
  password: { type: String, required: true }
});

/**
 * User model
 */
const USER = mongoose.model<IUser>("User", USER_SCHEMA);
export default USER;
