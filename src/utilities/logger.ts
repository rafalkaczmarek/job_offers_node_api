import morgan from "morgan";
import winston from "winston";
import { EnvironmentsEnum } from "../Environments.enum";

/**
 * Winston logger configuration.
 */
const logger = winston.createLogger({
  format: winston.format.json(),
  transports: [
    new winston.transports.Console({
      level:
        process.env.NODE_ENV.trim() === EnvironmentsEnum.PRODUCTION
          ? "error"
          : "debug"
    }),
    new winston.transports.File({
      filename: "debug.log",
      level: "debug"
    })
  ]
});

logger.stream = {
  // @ts-ignore
  write(message, encoding) {
    logger.info(message);
  }
};

if (process.env.NODE_ENV.trim() !== EnvironmentsEnum.PRODUCTION) {
  logger.debug("Logging initialized at debug level");
}

let loggerFormat = "combined";
if (process.env.NODE_ENV.trim() === "development") {
  loggerFormat = "dev";
}

/**
 * Morgan logger for success requests.
 */
const morganSuccess = morgan(loggerFormat, {
  skip(req, res) {
    return res.statusCode < 400;
  },
  stream: process.stderr
});

/**
 * Morgan logger for failure requests.
 */
const morganFailure = morgan(loggerFormat, {
  skip(req, res) {
    return res.statusCode >= 400;
  },
  stream: process.stdout
});

export { morganSuccess };
export { morganFailure };
export default logger;
