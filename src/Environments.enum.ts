/**
 * ENUM that contains available environments.
 */
export enum EnvironmentsEnum {
  /**
   * Production
   */
  PRODUCTION = 'production',
  /**
   * Development
   */
  DEVELOPMENT = 'development'
}