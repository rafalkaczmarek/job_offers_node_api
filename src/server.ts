import bluebird from "bluebird";
import bodyParser from "body-parser";
import errorhandler from "errorhandler";
import express from "express";
import expressBasicAuth from "express-basic-auth";
import mongoose from "mongoose";
import morgan from "morgan";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUiExpress from "swagger-ui-express";
import JOB_OFFER from "./models/JobOffer";
import USER from "./models/User";
import jobOffersRoutes from "./routes/jobOffersRoutes";
import usersRoutes from "./routes/usersRoutes";
import { MONGODB_URI } from "./utilities/configuration";
import logger, { morganFailure, morganSuccess } from "./utilities/logger";

const app = express();
// swagger definition
const swaggerDefinition = {
  basePath: "/",
  host: "localhost:3000",
  info: {
    description: "Demonstrating how to describe a RESTful API with Swagger",
    title: "Node Swagger API",
    version: "1.0.0"
  },
  securityDefinitions: {
    basicAuth: {
      type: "basic"
    }
  },
  security: [{ basicAuth: [] as any[] }]
};
// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["./src/routes/*.ts"]
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJsdoc(options);
// Swagger
app.use(
  "/swagger",
  swaggerUiExpress.serve,
  swaggerUiExpress.setup(swaggerSpec)
);
// serve swagger
app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

app.use(errorhandler());
app.use(morganSuccess);
app.use(morganFailure);

// @ts-ignore
app.use(morgan("combined", { stream: logger.stream }));

// Basic auth configuration
app.use(
  expressBasicAuth({
    // challenge: true,
    users: { admin: "admin" }
  })
);

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
(mongoose as any).Promise = bluebird;
mongoose
  .connect(
    mongoUrl,
    { useNewUrlParser: true }
  )
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch(err => {
    logger.error(
      `MongoDB connection error. Please make sure MongoDB is running. ${err}`
    );
    process.exit(1);
  });
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Models
const jobOfferModel = JOB_OFFER;
const userModel = USER;

usersRoutes.addUserRoutes(app);
jobOffersRoutes.addJobOfferRoutes(app);

// app.get("/", (req, res) => {
//   logger.debug("Debug statement");
//   logger.info("Info statement");
//   res.send("Hello World!");
// });

app.listen(3000, () => logger.info("Servers running on 3000!"));

// var express = require('express');
// var path = require('path');
// var cookieParser = require('cookie-parser');
// var logger = require('morgan');
//
// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
//
// var server = express();
//
// server.use(logger('dev'));
// server.use(express.json());
// server.use(express.urlencoded({ extended: false }));
// server.use(cookieParser());
// server.use(express.static(path.join(__dirname, 'public')));
//
// server.use('/', indexRouter);
// server.use('/users', usersRouter);
//
// module.exports = server;
