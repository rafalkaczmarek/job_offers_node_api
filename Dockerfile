FROM node:11.1.0-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install
#RUN npm install --only=production

COPY . .

EXPOSE 3000 3443

ENV NODE_ENV production


# Run express application.
CMD npm run start_production
#CMD ["npm", "run start_production"]
