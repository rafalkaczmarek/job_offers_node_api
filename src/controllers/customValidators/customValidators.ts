import moment from "moment";
import { JobCategoryEnum } from "../../models/JobOffer.enum";

/**
 * Custom date validator.
 * @param value
 */
const isValidDate = (value: string) => {
  if (moment(value, "YYYY-MM-DDTHH:mm:ss.SSS", true).isValid()) {
    return true;
  } // true
  return false;
};

/**
 * Check if value is one of the available job category.
 * @param value
 */
const isValidJobOfferCategory = (value: string) => {
  return !!(Object as any).values(JobCategoryEnum).includes(value);
};

export default { isValidDate, isValidJobOfferCategory };
