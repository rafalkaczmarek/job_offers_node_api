import jobOffersController from "../controllers/jobOffersController";

/**
 * Job offer routes
 * @param app
 */
const addJobOfferRoutes = (app: any) => {
  /**
   * @swagger
   *
   * definitions:
   *   NewJobOffer:
   *     type: object
   *     required:
   *       - title
   *       - category
   *       - offeringCompanyName
   *       - startingDate
   *       - endDate
   *     properties:
   *       title:
   *         type: string
   *       category:
   *         type: string
   *         format: it|food_and_drinks|office|courier|shop_assistant
   *       offeringCompanyName:
   *         type: string
   *       creationDate:
   *         type: string
   *         format: YYYY-MM-DDTHH:mm:ss.SSS
   *       endDate:
   *         type: string
   *         format: YYYY-MM-DDTHH:mm:ss.SSS
   *   JobOffer:
   *     allOf:
   *       - $ref: '#/definitions/NewJobOffer'
   *       - type: object
   *         required:
   *           - _id
   *           - ownerId
   *         properties:
   *           _id:
   *             type: string
   *           ownerId:
   *             type: string
   */
  // job offer Routes
  app
    .route("/offers")
    /**
     * @swagger
     * /offers:
     *   get:
     *     tags:
     *       - offers
     *     description: Returns job offers
     *     produces:
     *      - application/json
     *     responses:
     *       200:
     *         description: job offers
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/JobOffer'
     */
    .get(jobOffersController.listAllJobOffers);
};

export default { addJobOfferRoutes };
