import { Request, Response } from "express";
import moment from "moment";
import mongoose from "mongoose";
import { AppSettings } from "../AppSettings";
import { IJobOffer } from "../models/JobOffer";
import logger from "../utilities/logger";

const JOB_OFFER = mongoose.model("JobOffer");

/**
 * List all job offers
 * @param req
 * @param res
 */
const listAllJobOffers = (req: Request, res: Response) => {
  JOB_OFFER.find({}, (err, jobOffers: IJobOffer[]) => {
    if (err) {
      return res.status(400).send(err);
    }

    logger.info(
      `Query ${req.originalUrl} found ${jobOffers.length} job offers.`
    );
    jobOffers.forEach((jobOffer: IJobOffer) => {
      jobOffer.startingDate = moment
        .utc(jobOffer.startingDate)
        .format(AppSettings.DATE_TIME_SERVER_FORMAT);
      jobOffer.endDate = moment
        .utc(jobOffer.endDate)
        .format(AppSettings.DATE_TIME_SERVER_FORMAT);
    });

    return res.json(jobOffers);
  });
};

export default {
  listAllJobOffers
};
