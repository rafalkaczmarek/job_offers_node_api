import dotenv from "dotenv";
import fs from "fs";
import { EnvironmentsEnum } from "../Environments.enum";
import logger from "./logger";

if (fs.existsSync(`.env.${process.env.NODE_ENV.trim()}`)) {
  logger.info(
    `Using .env.${process.env.NODE_ENV.trim()} file to supply config environment variables`
  );
  dotenv.config({ path: `.env.${process.env.NODE_ENV.trim()}` });
} else {
  logger.error(
    `Cannot find .env.${process.env.NODE_ENV.trim()} file with environment variables.`
  );
}

export const ENVIRONMENT = process.env.NODE_ENV.trim();
const prod = ENVIRONMENT === EnvironmentsEnum.PRODUCTION; // Anything else is treated as 'dev'

export const SESSION_SECRET = process.env.SESSION_SECRET;
export const MONGODB_URI = prod
  ? process.env.MONGODB_URI
  : process.env.MONGODB_URI_LOCAL;

if (!SESSION_SECRET) {
  logger.error("No client secret. Set SESSION_SECRET environment variable.");
  process.exit(1);
}

if (!MONGODB_URI) {
  logger.error(
    "No mongo connection string. Set MONGODB_URI environment variable."
  );
  process.exit(1);
}
