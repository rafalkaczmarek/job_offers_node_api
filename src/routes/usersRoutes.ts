import usersController from "../controllers/usersController";

/**
 * User routes
 * @param app
 */
const addUserRoutes = (app: any) => {
  /**
   * @swagger
   *
   * definitions:
   *   NewUser:
   *     type: object
   *     required:
   *       - login
   *       - password
   *       - creationDate
   *     properties:
   *       login:
   *         type: string
   *       password:
   *         type: string
   *         format: password
   *       creationDate:
   *         type: string
   *         format: YYYY-MM-DDTHH:mm:ss.SSS
   *   User:
   *     allOf:
   *       - $ref: '#/definitions/NewUser'
   *       - type: object
   *         required:
   *           - _id
   *           - offers
   *         properties:
   *           _id:
   *             type: string
   *           offers:
   *             type: array
   *             description: job offers ids
   *             items:
   *               type: string
   */
  // users Routes
  app
    .route("/users")
    /**
     * @swagger
     * /users:
     *   get:
     *     tags:
     *       - users
     *     description: Returns users
     *     produces:
     *      - application/json
     *     responses:
     *       200:
     *         description: users
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/User'
     */
    .get(usersController.listAllUsers)
    /**
     * @swagger
     *
     * /users:
     *   post:
     *     tags:
     *       - users
     *     description: Creates a user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: user
     *         description: User object
     *         in:  body
     *         required: true
     *         type: string
     *         schema:
     *           $ref: '#/definitions/NewUser'
     *     responses:
     *       201:
     *         description: New user created
     *         schema:
     *           $ref: '#/definitions/User'
     *       409:
     *         description: User already exists
     */
    .post(usersController.createUserValidation, usersController.createUser);

  app
    .route("/users/:userId")
    /**
     * @swagger
     * /users/{userId}:
     *   get:
     *     tags:
     *       - users
     *     description: Returns a single user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: userId
     *         description: User id
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: A single user
     *         schema:
     *           $ref: '#/definitions/User'
     *       404:
     *         description: User not exists
     */
    .get(usersController.readUser)
    /**
     * @swagger
     *
     * /users/{userId}:
     *   put:
     *     tags:
     *       - users
     *     description: Creates a user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: userId
     *         description: User id
     *         in: path
     *         required: true
     *         type: string
     *       - name: user
     *         description: User object
     *         in:  body
     *         required: true
     *         type: string
     *         schema:
     *           $ref: '#/definitions/NewUser'
     *     responses:
     *       200:
     *         description: users
     *         schema:
     *           $ref: '#/definitions/User'
     *       409:
     *         description: User already exists
     */
    .put(usersController.createUserValidation, usersController.updateUser)
    /**
     * @swagger
     * /users/{userId}:
     *   delete:
     *     tags:
     *       - users
     *     description: Deletes a single user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: userId
     *         description: user's id
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: Successfully deleted
     */
    .delete(usersController.deleteUser);

  app
    .route("/users/:userId/offers")
    /**
     * @swagger
     *
     * /users/{userId}/offers:
     *   get:
     *     tags:
     *       - users
     *     description: Get a job offers for user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: userId
     *         description: User id
     *         in: path
     *         required: true
     *         type: string
     *       - name: category
     *         description: Job offer category/categories (use space as a delimiter)
     *         in: query
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: job offers
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/JobOffer'
     */
    .get(usersController.readUserOffers)
    /**
     * @swagger
     *
     * /users/{userId}/offers:
     *   post:
     *     tags:
     *       - users
     *     description: Creates a job offer
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: userId
     *         description: User id
     *         in: path
     *         required: true
     *         type: string
     *       - name: offer
     *         description: Job offer object
     *         in:  body
     *         required: true
     *         type: string
     *         schema:
     *           $ref: '#/definitions/NewJobOffer'
     *     responses:
     *       201:
     *         description: New job offer created
     *         schema:
     *           $ref: '#/definitions/JobOffer'
     */
    .post(
      usersController.createJobOfferValidation,
      usersController.createJobOffer
    );
};

export default { addUserRoutes };
